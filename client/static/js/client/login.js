function statusChangeCallback(response, login) {

  if (response.status === 'connected') {

    FB.api('/me', { locale: 'en_US', fields: 'first_name, last_name, email' }, function(response) {
      console.log(response);
      document.getElementById('fb_id').value = response.id;
      document.getElementById('fb_email').value = response.email;
      document.getElementById('fb_first_name').value = response.first_name;
      document.getElementById('fb_last_name').value = response.last_name;
      if ("{{request.method}}" != "POST" && login) {
        document.getElementById('fb_details').submit();
      }
    });
  }

}

function changeLoginState() {
  FB.login(function(response){

    FB.getLoginStatus(function(response) {
      statusChangeCallback(response, true);
    });
  }, {scope: 'public_profile,email'});
}

window.fbAsyncInit = function() {
  FB.init({
    appId      : '193692947707115',
    cookie     : true,  
    xfbml      : true,  
    version    : 'v2.8'
  });

  FB.getLoginStatus(function(response) {
    statusChangeCallback(response, false);
  });

};

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));