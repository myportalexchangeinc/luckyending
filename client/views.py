import uuid

from django.shortcuts import render, redirect, get_object_or_404
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from paypal.standard.forms import PayPalPaymentsForm

from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse  # 1.10
# from django.core.urlresolvers import reverse #1.9

from django.contrib.auth.models import User
from django.db.models import Sum

from datetime import datetime
from django.utils import timezone

from .models import *
from .forms import *

from hq.models import Game

import hashlib

# Create your views here.
import logging

LOGGER = logging.getLogger(__name__)


def get_client_info(client_id):
    client = get_object_or_404(Client, pk=client_id)
    wallet_pos = Transaction.objects.filter(client=client, method='C').aggregate(Sum('wallet'))['wallet__sum']
    free_credits_pos = Transaction.objects.filter(client=client, method='C').aggregate(Sum('free_credits'))[
        'free_credits__sum']
    wallet_neg = Transaction.objects.filter(client=client, method='D').aggregate(Sum('wallet'))['wallet__sum']
    free_credits_neg = Transaction.objects.filter(client=client, method='D').aggregate(Sum('free_credits'))[
        'free_credits__sum']

    if wallet_pos is None or wallet_neg is None:
        wallet = 0
    else:
        wallet = wallet_pos - wallet_neg
    if free_credits_pos is None or free_credits_neg is None:
        free_credits = 0
    else:
        free_credits = free_credits_pos - free_credits_neg

    return {'client': client, 'wallet': wallet, 'free_credits': free_credits}

def client_transactions(request, client_id):
    client = get_object_or_404(Client, pk=client_id)
    transactions = Transaction.objects.filter(client=client).order_by('-id')
    return render(request, 'client/transactions.html', {'transactions': transactions})


def index(request):
    print("LANDING PAGE", request)
    return render(request, 'client/index.html', {})


def fb_login(request):
    form = AuthenticationForm()

    if request.method == 'POST':
        facebook = request.POST.get('facebook')

        if facebook:
            try:
                this_user = Client.objects.get(fb_id=str(request.POST.get('fb_id'))).user

            except Exception as e:
                fb_details = {
                    "id": str(request.POST.get('fb_id')),
                    "email": str(request.POST.get('fb_email')),
                    "first_name": str(request.POST.get('fb_first_name')),
                    "last_name": str(request.POST.get('fb_last_name')),
                }
                request.session['facebook'] = fb_details
                return HttpResponseRedirect(reverse('client:registration'))

        else:
            form = AuthenticationForm(data=request.POST)
            this_user = None

            if form.is_valid():
                this_user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
            else:
                return render(request, 'client/login.html', {"form": form})

        if this_user is not None:
            login(request, this_user)
            print('next:', request.POST.get('next'))
            if request.POST.get('next'):
                return redirect(request.POST.get('next'))
            elif this_user.is_staff:
                return redirect('hq:games')
            return redirect('client:home')

    else:
        form = AuthenticationForm()

    return render(request, 'client/login.html', {"form": form})


def hash_facebook_id(instance):
    salt = 'randomizing passwords for facebook users'
    hash = hashlib.md5((str(instance) + salt).encode('utf-8')).hexdigest()
    return str(hash)


def registration(request):
    facebook = None
    post = {}

    try:
        facebook = request.session.get('facebook')
        del request.session['facebook']
    except Exception as e:
        pass

    if request.method == 'POST':
        get = False
        via_facebook = request.POST.get('facebook') == 'True'

        client_form = ClientCreateForm(request.POST)
        user_form = UserCreateForm(request.POST)

        if via_facebook:
            facebook = {
                "id": str(request.POST.get('fb_id')),
                "email": str(request.POST.get('fb_email')),
                "first_name": str(request.POST.get('fb_first_name')),
                "last_name": str(request.POST.get('fb_last_name')),
            }

        if user_form.is_valid() and client_form.is_valid():

            password = user_form.cleaned_data['password1']

            if via_facebook:
                password = hash_facebook_id(str(request.POST.get('fb_id')))

            this_user = User.objects.create_user(username=user_form.cleaned_data['email'],
                                                 email=user_form.cleaned_data['email'], password=password)
            this_user.save()

            new_client = Client(user=this_user)
            client_form = ClientCreateForm(request.POST, instance=new_client)
            this_client = client_form.save()

            login(request, this_user)

            return HttpResponseRedirect(reverse('client:home'))

        try:
            post['username'] = user_form.cleaned_data['username']
        except Exception as e:
            pass
        try:
            post['email'] = user_form.cleaned_data['email']
        except Exception as e:
            pass
        try:
            post['first_name'] = client_form.cleaned_data['first_name']
        except Exception as e:
            pass
        try:
            post['last_name'] = client_form.cleaned_data['last_name']
        except Exception as e:
            pass
        try:
            post['contact_number'] = client_form.cleaned_data['contact_number']
        except Exception as e:
            pass


    else:
        user_form = UserCreateForm()
        client_form = ClientCreateForm()
        get = True

    return render(request, 'client/registration.html',
                  {"user_form": user_form, "client_form": client_form, "facebook": facebook,
                   "get": (request.method == 'GET'), "post": post})

def paypal_sample(request):
    # What you want the button to do.
    print(reverse('paypal-ipn'))
    LOGGER.info("Derp")
    paypal_dict = {
        "business": "ivanajoyce.c-facilitator@gmail.com",
        "amount": "100.00",
        "currency_code": " PHP",
        "item_name": "LuckyEnding credits",
        "invoice": str(uuid.uuid4()),
        # "notify_url": "http://requestb.in/s319yus3",

        "notify_url": "http://jinjjabread.com" + reverse('paypal-ipn'),
        "return_url": "http://jinjjabread.com/paypal_sample?status=return",
        "success_url": "http://jinjjabread.com/paypal_sample?status=success",
        "cancel_return": "http://jinjjabread.com/paypal_sample?status=cancel",
        "custom": "Upgrade all users!",  # Custom command to correlate to some function later (optional)
    }
    client_info = get_client_info(client_id=request.user.client.id)
    form = PayPalPaymentsForm(initial=paypal_dict)

    client = get_object_or_404(Client, pk=request.user.client.id)
    transactions = Transaction.objects.filter(client=client).order_by('-id')

    context = {"form": form, "client_info": client_info, "transactions": transactions}

    return render(request, "client/paypal.html", context)


def games(request):
    games = Game.objects.all().order_by('-id')
    client_info = get_client_info(client_id=request.user.client.id)
    credits = client_info['wallet'] + client_info['free_credits']
    return render(request, 'client/games.html', {'games': games, 'today': timezone.now, 'credits': credits })

def bet(request, game_id):
    game = get_object_or_404(Game, pk=game_id)
    client_info = get_client_info(client_id=request.user.client.id)
    credits = client_info['wallet'] + client_info['free_credits']

    return render(request, 'client/bet.html', {'game': game, 'credits': credits})

def screen_size(request):
    return render(request, 'client/screen.html', {})