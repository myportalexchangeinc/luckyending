from django import forms
from .models import *
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

from django.core.validators import RegexValidator, EmailValidator

from django.core.exceptions import ValidationError

def new_username(username):
    try:
        user = User.objects.get(username=username)
    except User.DoesNotExist:
        user = None
    if user: # check if any object exists
        raise ValidationError('Email address is already registered.') 

class UserCreateForm(UserCreationForm):
    email = forms.CharField(validators=[EmailValidator, new_username])

    class Meta:
        model = User
        fields = ['username', 'password1', 'password2', 'email']

class ClientCreateForm(forms.ModelForm):
    contact_number = forms.CharField(validators=[RegexValidator(r'^[0-9]{12}', 'Enter a valid number.')])

    class Meta:
        model = Client
        exclude = ['user']