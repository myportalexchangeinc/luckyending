from django import forms
from .models import *
from client.models import *

# from bootstrap3_datetime.widgets import DateTimePicker

class GameForm(forms.ModelForm):
    schedule = forms.DateTimeField(
        input_formats=[
            '%Y-%m-%d %H:%M:%S',    # '2006-10-25 14:30:59'
            '%Y-%m-%d %H:%M',       # '2006-10-25 14:30'
            '%Y-%m-%d',             # '2006-10-25'
            '%m/%d/%Y %H:%M:%S',    # '10/25/2006 14:30:59'
            '%m/%d/%Y %H:%M',       # '10/25/2006 14:30'
            '%m/%d/%Y',             # '10/25/2006'
            '%m/%d/%y %H:%M:%S',    # '10/25/06 14:30:59'
            '%m/%d/%y %H:%M',       # '10/25/06 14:30'
            '%m/%d/%y',             # '10/25/06'
            '%Y/%m/%d %I:%M %p',     # '2006/10/25 2:30 PM'
            '%Y-%m-%d %I:%M %p'     # '2006/10/25 2:30 PM'
        ],
        # widget=DateTimePicker(
        #     options={
        #         "format": "YYYY-MM-DD h:mm a",
        #         "pickSeconds": False
        #     }
        # ),
    )

    class Meta:
        model = Game
        fields = '__all__'

class ScoreForm(forms.ModelForm):
   class Meta:
        model = Score
        fields = '__all__'

class ServiceCreditForm(forms.ModelForm):
    class Meta:
        model = ServiceCredit
        fields = '__all__'

class RegularCreditForm(forms.ModelForm):
    class Meta:
        model = RegularCredit
        fields = '__all__'

class CashoutUpdateForm(forms.ModelForm):
    class Meta:
        model = Cashout
        fields = ('status', 'reference', 'staff')
