from django.dispatch import receiver
from paypal.standard.ipn.models import PayPalIPN
from paypal.standard.models import ST_PP_COMPLETED, ST_PP_PENDING
from paypal.standard.ipn.signals import valid_ipn_received, invalid_ipn_received

import logging

LOGGER = logging.getLogger(__name__)


@receiver(valid_ipn_received)
def handle_ipn(sender, **kwargs):
    ipn_obj = sender
    LOGGER.info("Payment Confirmation received: " + ipn_obj.txn_id)
    LOGGER.info("Payment Status:" + ipn_obj.payment_status)
    if ipn_obj.payment_status == ST_PP_COMPLETED:
        # WARNING !
        # Check that the receiver email is the same we previously
        # set on the business field request. (The user could tamper
        # with those fields on payment form before send it to PayPal)
        if ipn_obj.receiver_email != "adrian-facilitator@txtcircuit.com":
            # Not a valid payment
            LOGGER.info("PAYMENT NOT VALID")
            return

        # ALSO: for the same reason, you need to check the amount
        # received etc. are all what you expect.

        # Undertake some action depending upon `ipn_obj`.
        # if ipn_obj.custom == "Upgrade all users!":
        #     Users.objects.update(paid=True)
        LOGGER.info("Payment is Valid! :D")
    elif ipn_obj.payment_status == ST_PP_PENDING:
        LOGGER.info("Payment is status is pending for:" + ipn_obj.txn_id)
    else:
        # ...
        LOGGER.info("Payment Confirmation not completed")


@receiver(invalid_ipn_received)
def handle_bad_ipn(sender, **kwargs):
    LOGGER.info("Bad Payment Confirmation received")

# @receiver(startup_received)
# def handle_startup(sender, **kwargs):
#     LOGGER.info("STARTED")
#     print("STARTED")

# ST_PP_ACTIVE = 'Active'
# ST_PP_CANCELLED = 'Cancelled'
# ST_PP_CANCELED_REVERSAL = 'Canceled_Reversal'
# ST_PP_CLEARED = 'Cleared'
# ST_PP_COMPLETED = 'Completed'
# ST_PP_CREATED = 'Created'
# ST_PP_DECLINED = 'Declined'
# ST_PP_DENIED = 'Denied'
# ST_PP_EXPIRED = 'Expired'
# ST_PP_FAILED = 'Failed'
# ST_PP_PAID = 'Paid'
# ST_PP_PENDING = 'Pending'
# ST_PP_PROCESSED = 'Processed'
# ST_PP_REFUNDED = 'Refunded'
# ST_PP_REFUSED = 'Refused'
# ST_PP_REVERSED = 'Reversed'
# ST_PP_REWARDED = 'Rewarded'
# ST_PP_UNCLAIMED = 'Unclaimed'
# ST_PP_UNCLEARED = 'Uncleared'
# ST_PP_VOIDED = 'Voided'
