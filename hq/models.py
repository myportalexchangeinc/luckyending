from django.db import models
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from django.utils import timezone
from datetime import datetime, timedelta

# Create your models here.

class Game(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    action_by = models.ForeignKey(User)
    schedule = models.DateTimeField()
    team_a = models.CharField(max_length=64, blank=True, null=True)
    team_b = models.CharField(max_length=32)
    #status = scored, pending score, open (allow bet), closed (stop bet)

    def __str__(self):
        return self.team_a + " vs " + self.team_b

    def __init__(self, *args, **kwargs):
        super(Game, self).__init__(*args, **kwargs)
        self.__original_schedule = self.schedule
        self.__original_team_a = self.team_a
        self.__original_team_b = self.team_b
        self.changed_from = {'key': 'item'}

    def save(self, *args, **kwargs):
        changed_from = {}
        if self.schedule != self.__original_schedule:
            changed_from['schedule'] = self.__original_schedule
        if self.team_a != self.__original_team_a:
            changed_from['team_a'] = self.__original_team_a
        if self.team_b != self.__original_team_b:
            changed_from['team_b'] = self.__original_team_b

        self.changed_from = changed_from

        super(Game, self).save(*args, **kwargs)
        self.__original_schedule = self.schedule
        self.__original_team_a = self.team_a
        self.__original_team_b = self.team_b

    def status(self):
        delta = self.schedule - timedelta(hours=1) #close 1 hour before
        print("DELTA", delta)
        print("NOW()", timezone.now())
        left = delta - timezone.now()
        days = left.days
        hours = (left.seconds//3600)
        minutes = ((left.seconds//60)%60)
        time = ""
        if days < 100:
            time+="0"
        if days < 10:
            time+="0"
        time+=str(days)+":"
        if hours < 10:
            time+="0"
        time+=str(hours)+":"
        if minutes < 10:
            time+="0"
        time+=str(minutes)
        if timezone.now() < delta:
            return {"description":"OPEN", "days_left":days, "hours_left":hours, "minutes_left":minutes, "time_left":time}
        else:
            return {"description":"CLOSED", "time_left": "000:00:00"}

    def hours_left(self):
        delta = self.schedule - timedelta(hours=1) #close 1 hour before
        print(delta)
        print(timezone.now())
        left = delta - timezone.now()
        print(left)
        if timezone.now() < delta:
            return "OPEN"
        else:
            return "CLOSED"


class Score(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    action_by = models.ForeignKey(User)
    game = models.ForeignKey(Game)
    score_a = models.IntegerField(blank=True, null=True)
    score_b = models.IntegerField(blank=True, null=True)

    class Meta:
        unique_together = ('action_by', 'game')

    def __str__(self):
        return str(self.score_a) + ":" + str(self.score_b) + " (" + str(self.game) + ")"

    def __init__(self, *args, **kwargs):
        super(Score, self).__init__(*args, **kwargs)
        self.__original_score_a = self.score_a
        self.__original_score_b = self.score_b
        self.changed_from = {'key': 'item'}

    def save(self, *args, **kwargs):
        changed_from = {}
        if self.score_a != self.__original_score_a:
            changed_from['score_a'] = self.__original_score_a
        if self.score_b != self.__original_score_b:
            changed_from['score_b'] = self.__original_score_b

        self.changed_from = changed_from

        super(Score, self).save(*args, **kwargs)
        self.__original_score_a = self.score_a
        self.__original_score_b = self.score_b

class StaffActivity(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    staff = models.ForeignKey(User)
    content_type = models.ForeignKey(ContentType, related_name="staff_activity")
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    ACTION_CHOICES = (
        ('C', 'Create'),
        ('U', 'Update'),
    )
    action = models.CharField(
        max_length=1,
        choices=ACTION_CHOICES,
        default='C',
    )
    details = models.TextField()

    class Meta:
        verbose_name_plural = 'staff activities'

from django.db.models.signals import post_save
from django.dispatch import receiver

@receiver(post_save, sender=Game)
@receiver(post_save, sender=Score)
def log_post_activity(sender, instance, created, update_fields, **kwargs):
    if created:
        content_type = ContentType.objects.get_for_model(instance)
        details = ""

        if content_type == ContentType.objects.get_for_model(Game):
            details = "[Created game:] " + str(instance)
        elif content_type == ContentType.objects.get_for_model(Score):
            details = "[Updated score:] " + str(instance)

        activity = StaffActivity(staff=instance.action_by, content_type=content_type, object_id = instance.id, content_object=instance, details=details)

        activity.save()

    else:
        content_type = ContentType.objects.get_for_model(instance)
        details = ""

        if content_type == ContentType.objects.get_for_model(Game):
            details = "[Updated game:] " + str(instance)

            changes = ""

            for i, field in enumerate(instance.changed_from):
                if i > 0:
                    changes += "\n"
                changes += "Changed " + field + " from " + str(instance.changed_from[field]) + " to " + str(getattr(instance, field))

        elif content_type == ContentType.objects.get_for_model(Score):
            details = "[Edited score:] " + str(instance)
        
        activity = StaffActivity(staff=instance.action_by, content_type=content_type, object_id = instance.id, content_object=instance, action='U', details=details)

        activity.save()
