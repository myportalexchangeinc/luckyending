from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.home, name="home"),
    url(r'^staff/$', views.staff_activity, name="staff_activity"),
    url(r'^staff/create/$', views.create_staff, name="create_staff"),
    url(r'^games/$', views.games, name="games"),
    url(r'^score/create/$', views.create_score, name="score_game"),
    url(r'^services/credit/$', views.service_credit, name="service_credit"),
]