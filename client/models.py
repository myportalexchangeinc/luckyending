from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from hq.models import Game

# Create your models here.

class Client(models.Model):
    user = models.OneToOneField(User)
    fb_id = models.CharField(max_length=64, blank=True, null=True)
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=16)
    contact_number = models.CharField(max_length=12)

    def __str__(self):
        return str(self.user.email)

class Transaction(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
    client = models.ForeignKey(Client)
    wallet = models.PositiveIntegerField(default=0)
    free_credits = models.PositiveIntegerField(default=0)
    METHOD_CHOICES = (
        ('C', 'Credit'),
        ('D', 'Debit'),
    )
    method = models.CharField(
        max_length=1,
        choices=METHOD_CHOICES,
        default='C',
    )
    METHOD_TYPE_CHOICES = (
        ('SC', 'Service'),
        ('RC', 'Regular'),
        ('GC', 'Game'),
        ('BD', 'Bet'),
        ('CD', 'Cashout')
    )
    method_type = models.CharField(
        max_length=2,
        choices=METHOD_TYPE_CHOICES,
        default='RC',
    )

    def __str__(self):
        s = {'SC': 'SC for Client '+str(self.client.id), 'RC': 'RC for Client '+str(self.client.id), 'GC': 'GC for Client '+str(self.client.id), 'BD': 'Bet/s by Client '+str(self.client.id), 'CD': 'Cashout by Client '+str(self.client.id) }[self.method_type]
        return str(s)

class ServiceCredit(Transaction):
    staff = models.ForeignKey(User, blank=True, null=True)
    SERVICE_TYPE_CHOICES = (
        ('W', 'Welcome Credits'),
        ('P', 'Promo'),
        ('R', 'Referral'),
    )
    service_type = models.CharField(
        max_length=1,
        choices=SERVICE_TYPE_CHOICES,
        default='W',
    )
    details = models.CharField(max_length=64, blank=True, null=True)

class RegularCredit(Transaction):
    SOURCE_CHOICES = (
        ('P', 'Paypal'),
        ('O', 'Others'),
    )
    source = models.CharField(
        max_length=1,
        choices=SOURCE_CHOICES,
        default='W',
    )
    reference = models.CharField(max_length=32)

class GameCredit(Transaction):
    game = models.ForeignKey(Game)
    TRIGGER_CHOICES = (
        ('W', 'Win'),
        ('R', 'Refund'),
    )
    trigger = models.CharField(
        max_length=1,
        choices=TRIGGER_CHOICES,
        default='W',
    )

class Bet(models.Model):
    game = models.ForeignKey(Game)
    client = models.ForeignKey(Client)
    ending_a = models.PositiveIntegerField(validators=[MaxValueValidator(9),])
    ending_b = models.PositiveIntegerField(validators=[MaxValueValidator(9),])
    amount = models.PositiveIntegerField()
    transaction = models.ForeignKey(Transaction, blank=True, null=True)

class Cashout(Transaction):
    STATUS_CHOICES = (
        ('Pe', 'Pending Review'),
        ('Pr', 'Processing'),
        ('Re', 'Released'),
    )
    status = models.CharField(
        max_length=2,
        choices=STATUS_CHOICES,
        default='Pe',
    )
    staff = models.ForeignKey(User, blank=True, null=True)
    reference = models.CharField(max_length=32, blank=True, null=True)
    ACCOUNT_TYPE_CHOICES = (
        ('PPL', 'Paypal'),
        ('BPI', 'BPI'),
        ('BDO', 'BDO'),
    )
    account_type = models.CharField(
        max_length=3,
        choices=ACCOUNT_TYPE_CHOICES,
        default='PPL',
    )
    account_details =models.TextField()
