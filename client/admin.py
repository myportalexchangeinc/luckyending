from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Client)
admin.site.register(Transaction)
admin.site.register(ServiceCredit)
admin.site.register(RegularCredit)
admin.site.register(GameCredit)
admin.site.register(Bet)
admin.site.register(Cashout)