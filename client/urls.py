from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="home"),
    url(r'^login/$', views.fb_login, name="login"),
    url(r'^payment_sample/$', views.paypal_sample, name="paypal_sample"),
    url(r'^games/$', views.games, name="games"),
    url(r'^games/(?P<game_id>[0-9]+)/$', views.bet, name="bet"),
    url(r'^register/$', views.registration, name="registration"),
    #url(r'^client/(?P<client_id>[0-9]+)/$', views.client_transactions, name="client_transactions"),
    url(r'^screen/$', views.screen_size)
]