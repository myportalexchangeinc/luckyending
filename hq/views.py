from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import PermissionDenied

from .models import *
from .forms import *

# Create your views here.

def is_admin(user):
    test = user.groups.filter(name='Admin').exists() or user.is_superuser

    if test:
        return True
    else:
        raise PermissionDenied

def is_staff_member(user):
    print(user)
    print(user.groups.filter(name__in=['Staff', 'Admin']).exists())
    test = user.groups.filter(name__in=['Staff', 'Admin']).exists() or user.is_superuser

    if test:
        return True
    else:
        raise PermissionDenied

@login_required(redirect_field_name='next', login_url='client:login')
@user_passes_test(is_admin)
def create_staff(request):
    
    if request.method == 'POST':
        form = UserCreationForm(request.POST)

        if form.is_valid():
            u = form.save()

            u.is_staff = True
            g = Group.objects.all().filter(name='Staff').first()
            u.groups.add(g)

            u.save()

            return HttpResponseRedirect(reverse('client:home'))

    else:
        form = UserCreationForm()

    return render(request, 'hq/staff_create.html', {'form': form})

@login_required(redirect_field_name='next', login_url='client:login')
@user_passes_test(is_admin)
def staff_activity(request):
    
    activities = StaffActivity.objects.all().order_by('-id')

    return render(request, 'hq/staff_activity.html', {'activities': activities})

@login_required(redirect_field_name='next', login_url='client:login')
@user_passes_test(is_staff_member)
def games(request):
    
    games = Game.objects.all().order_by('-id')

    if request.method == 'POST':
        form = GameForm(request.POST)

        if form.is_valid():
            
            game = form.save()
            print(game)
            return HttpResponseRedirect(reverse('hq:games'))

    else:
        form = GameForm()

    return render(request, 'hq/games.html', {'games': games, 'form': form})

@login_required(redirect_field_name='next', login_url='client:login')
@user_passes_test(is_staff_member)
def create_score(request):
    
    if request.method == 'POST':
        form = ScoreForm(request.POST)

        if form.is_valid():
            
            score = form.save()
            return HttpResponseRedirect(reverse('hq:staff_activity'))

    else:
        form = ScoreForm()

    return render(request, 'hq/score_create.html', {'form': form})

@login_required(redirect_field_name='next', login_url='client:login')
@user_passes_test(is_staff_member)
def service_credit(request):
    
    if request.method == 'POST':
        form = ServiceCreditForm(request.POST)

        if form.is_valid():
            
            score = form.save()
            return HttpResponseRedirect(reverse('hq:staff_activity'))

    else:
        form = ServiceCreditForm()

    return render(request, 'hq/service_credit.html', {'form': form})

@login_required(redirect_field_name='next', login_url='client:login')
@user_passes_test(is_staff_member)
def home(request):
    return redirect('hq:games')
